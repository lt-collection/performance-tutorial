---
marp: true
---

# Webサイトパフォーマンス改善チュートリアル（拡大版）

---

## （自分を追い込むための）宣伝

---

### 9月22日（日）技術書典に出ます！！

[Web技術の闇鍋本](https://techbookfest.org/event/tbf07/circle/5750034561761280)

（木達さんともんどさんが書くよ！！）

（確約できないんですが欲しい人いましたら取り置いときます）

![bg left contain](./images/img-4.png)

---

### 9月18日（水）PWA nightsのLT枠に出ます！！

[PWA Night vol.8 ～ここからはじめよう。初心者回～](https://pwanight.connpass.com/event/144224/)

（どっからみるかわかんないんですけど中継やってるっぽいので興味あるひとぜひ）

（初心者回なのでPWA？？なにそれ？？おいしいの？？の人もきっとたのしい）

![bg left contain](./images/img-3.png)

---

## とりあえずチューニングいっとこ

[Optimize Website Speed With Chrome DevTools  |  Tools for Web Developers  |  Google Developers](https://developers.google.com/web/tools/chrome-devtools/speed/get-started)

トニーが運営してるサイトの速度改善をします。

---

### 1. Auditでサイトを計測する

1. [Glitch :･ﾟ✧](https://glitch.com/edit/#!/tony)を開く
2. Show New Windowを選択すると[Tony's Favorite Foods](https://tony.glitch.me/)が表示される
3. Command+Option+J (Mac) Control+Shift+J (Windows, Linux, Chrome OS)でDev ToolsのConsoleが表示される
4. Command+Shift+P (Mac) or Control+Shift+P (Windows, Linux, Chrome OS) でコマンドメニューを開き、「selecting Undock into separate window」を選択

---

### 2. Lighthouseを眺めてみよう

- Metrics：サイトの計測結果の定量的な測定値が表示される
- Opportunities：どうやったらサイトのパフォーマンスを向上させられるかのTips
- Diagnostics：ページの読み込み時間に影響を与える要因に関する詳細情報

「Opportunities」「Diagnostics」があるからスコアが悪い（´・ω・｀）、のではなくて、「Metrics」が悪いからスコアが悪い。「Opportunities」「Diagnostics」はあくまでGoogleさんからのアドバイス

---

### 3. 画像を圧縮しよう！

- 「Enable Text Compression」
- Network＞ Use Large Request Rows＞サイズでソート
- `bundle.js`が1.4MB、Headersを見てみると`content-encoding`がない。よって`bundle.js`が圧縮されてない
- あとキャッシュも見てみるといいかも

---

#### 改善方法

- 圧縮してみる（`server.js`をいじる）
- またNetworkパネル見てみると、サイズが圧縮されてるはず

```javascript
const fs = require('fs');
const compression = require('compression');
app.use(compression());
app.use(express.static('build'));
```

---

### 4. 読み込みの最適化

- 「Avoids Enormous Network Payloads」
    - ネットワーク要求の合計サイズを小さくするとページの読み込み時間が短縮される

---

#### 改善方法

- 画像サイズを適切に変更する
    - `src/modal.js`を開き、`const dir = big`を`const dir = small`に。
- 小さいアプリなら画像小さくする、で十分
- 大きなアプリは以下のことをしてみてもよき
    - [画像最適化はこちらの記事を読むのがよき](https://images.guide/)
        - 画像最適化自動化すべき
    - ビルドの間にリサイズする
    - `srcset`でデバイスの解像度ごとに画像を切り替える
    - [webp導入を検討する](https://caniuse.com/#feat=webp)
        - [Using WebP Images](https://css-tricks.com/using-webp-images/)
    - CDNで配信する

---

### 5. レンダリングを妨げるリソース​

-「Eliminate render-blocking resources」
- レンダリングを妨げるリソース
    - Command+Shift+P (Mac) or Control+Shift+P (Windows, Linux, Chrome OS)でShow Coverageを選択
        - Coverageタブが開くのでReloadを押す。どこが読み込まれてないのかわかる

---

#### 改善案

- ほんとにこれらが必要かテストする
    - ネットワークタブを開く
    - コマンドパレットから「blockling」を押す。そうするとリクエストブロッキングタブが開く
    - ＋ボタンをクリックし`/libs/*`をタイプ
    - ページをリロードする
    - いらなそうなので `<script src="/libs/lodash.js"> `と`<script src="/libs/jquery.js"></script>`を消す
    - Auditsでどんな感じか見てみる

---

![](./images/img-5.svg)

- クリティカルレンダリングパスをチューニングするには
    - 完全に削除できるスクリプトを見つけることはほとんどない
    - 多くのスクリプトはページのロード中に要求する必要がなく`defer`の読み込みで対応してみるといい
    - フレームワークを使用している場合は、プロダクションモードを利用する。重要なレンダリングを妨げている不要なコードを排除するために、Three Shakingなどの機能を使用できる

---

### 6. メインスレッドの最適化

- 「Minimize main-thread work」
- メインスレッドは、HTML、CSS、およびJavaScriptの解析と実行など、ブラウザがページを表示するために必要な作業のほとんどを実行する場所
    - 目的は、パフォーマンスパネルを使用して、ページの読み込み中にメインスレッドが実行している作業を分析し、不要な作業を延期または削除する方法を見つけること

---

1. Performanceタブをクリック
2. SettingでSlow 3G、CPU to 6x slowdown
3. リロード
4. 赤枠の面グラフ部分が何かの色で埋まってるところがCPUがとてもがんばってるところ
5. `MainBecon`が大量に起こされているのがわかる
  - ボトムアップセクションを展開する。このタブではどのアクティビティについてもっとも時間がかかったか表示される
  - [ボトムアップ]セクションには、現在選択している活動または活動のグループに関する情報のみが表示される

---

#### 改善方法

- webpack.config.jsでモードを「production」に
- App.jsxでBitCoin関数を削除

---

### まとめ

- 元記事とちょっとちがう
- メトリクスしょっちゅうかわるつらい
    - [新しくメトリクスが追加される気配](https://github.com/GoogleChrome/lighthouse/releases)
        - 「Total Blocking Time」
            - FCPとTTIの間っぽい
- Lighthouse v5.2（Chrome 77）から「Third-Party Usage」がみれるようになるよ
- あくまでもGoogle DevToolsは「遅くなる要因がないかどうか」の確認
- フロントエンドのWebパフォーマンスは「圧縮」と「最適化」

---

## おまけ（Lighthouseのメトリクスについて）

---

### Webサイトパフォーマンス？？？

[Webパフォーマンスの管理用特性要因図](https://qiita.com/takehora/items/6b047a7d5e1a3a5f8d13#%E7%AE%A1%E7%90%86%E7%94%A8%E7%89%B9%E6%80%A7%E8%A6%81%E5%9B%A0%E5%9B%B3%E3%82%92%E4%BD%9C%E3%81%A3%E3%81%A6%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E3%81%99%E3%82%8B)

![bg contain right](./images/img-1.png)

---

> ダウンロード時間＝DNS Lookup + Initial Connection + SSL Negotiatin + First Byte Download + Content Download + Client Time

- DNS Lookup：DNSの名前解決にかかった時間
- Initial Connection：TCP/IP 3way handshakeにかかった時間
- SSL Negotiation：SSLを使っている場合、SSLセッション確立までにかかった時間
- First Byte Download：HTTP Get/Postのリクエストが発呼されて、その応答の1Byteめが届くまでの時間
- Content Download：HTTP Get/Postのリクエストの応答の最後のByteが届いて完了するまでの時間
- Client Time：ブラウザ側で処理にかかった時間

---

### Webサイトのメトリクス

<https://twitter.com/JoaoCunha/status/1087523708665765889>

![bg right contain](./images/img-2.jpg)

---

### Lighthouse v5.1のメトリクス

（あくまでもひとつのサンプルの速度にすぎない）

- Lighthouseで表示される[メトリクス](https://web.dev/lighthouse-performance)にはそれぞれ[スコア](https://docs.google.com/spreadsheets/d/1Cxzhy5ecqJCucdf1M0iOzM8mIxNc7mmx107o5nj38Eo/edit#gid=0)があって、重み付けがされてる
    - 1X - first meaningful paint
    - 2X - first cpu idle
    - 5X - time to interactive
    - 4X - speed index
    - 0X - estimated input latency
- Oppotunitiesとかは上記のスコアを改善するためのヒントにすぎない

---

#### First Contentful Paint

- ナビゲーションからブラウザがDOMのコンテンツの最初のビットをレンダリングするまでの時間を測定
- ページが実際にロードされているというフィードバックを提供するため、ユーザーにとって重要なマイルストーン

---

#### 改善方法

- リソースのダウンロード時間を高速化
- ブラウザがDOMコンテンツをレンダリングするのを妨げる作業を減らす

---

#### Lighthouseで見てみる方法

- Gzip配信されてるか（cache-encoding）
    - HTTP Caching（cache-control）
    - no-cache／no-store／public／private／max-age

---

- Waterfall
    - Waiting(TTFB) > ContentDownloadになっ   てないか
    - リソース読み込みについて、左の灰色の線がめっちゃ長くないか
        - 左側の灰色の直線：リクエストを送信するまでの時間。サーバーとの接続に時間がかかったり、他に優先度の高い処理を行っているとこの時間が長くなります。
        - 薄い色の四角形：リクエストを送信し、最初のデータを受信するまでの時間（TTFB = Time To First Byte)。サーバーの処理時間や、クライアントとサーバー間のネットワーク経路の状態、物理的な距離によって長くなる
        - 濃い色の四角形：TTFBからすべてのデータを受信するまでの時間。データのサイズが大きいと長くなる
        - 右側の灰色の直線：データ受信後、メインスレッドがデータを処理するまでの時間。メインスレッドが他の処理を行っているとこの時間が長くなる

---

- Coverage見る
    - 不要なファイル読み込んでないかな？？？

---

### First Meaningful Paint

- ユーザーがページの主要コンテンツの表示までの時間

---

#### 改善方法

- クリティカルレンダリングパスを最適化する
    - HTML、CSS、JavaScriptのバイトの受信から、これらをピクセルとしてレンダリングするために必要な処理までの中間段階で行われている内容
    - HTMLパースし、CSSOMツリーをパースし、スクリプトを処理し、ペイントするまで
- JavaScriptの最適化、HTMLの最適化、CSSの最適化
    - 圧縮、分割
    - CSSはレンダリングブロックリソースです。できるだけ早くクライアントに配信して、最初のレンダリングまでの時間を最適化する必要があります。
    - JavaScriptは非同期読み込みさせる（defer）

---

#### Lighthouseでのみかた

- PerformanceパネルでHTMLパースが分断されていないかなど

---

#### Speed Index


- [ロード開始からファーストビューが如何に早く表示されるかを表すスコア](https://1000ch.net/posts/2017/speedindex-for-web-performance.html)
    - 秒数が一緒でも、途中経過が異なるとユーザーに違う印象を与える
    - 画像で取得してる

---

#### 改善方法

- レンダリングを改善する
    - クリティカルレンダリングパスの改善
- リソースを分割する
    - ページで利用しないリソースは読み込みしない

など

---

#### Lighthouseでのみかた

- Lighthouseのスクリーンショット、Perforomanceパネルのスクリーンショットなど

---

### First CPU Idle

- ナビゲーションが開始されてからページが最小限のインタラクティブになるまでの時間
- ページのメインスレッドがユーザーの入力を処理する準備が整ったとき（＝他の処理で色々忙しくない時）ページの読み込み時間を記録する
- [HTTParchiveの実際のWebサイトパフォーマンスデータのFirst CPU Idleと、First CPU Idleの速度の比較に基づいている](https://web.dev/first-cpu-idle#how-lighthouse-determines-your-first-cpu-idle-score)

---

#### 改善案

- メインスレッドの処理を最適化する

など

---

#### Lighthouseでのみかた

- Performanceパネルでスレッドの動作を確認する

---

### Time to Interactive

- ページが完全にインタラクティブになるまでの時間
    - ここでいうInteractiveとは以下：
        - FCPで計測されるような有用なコンテンツが読み込まれたとき
        - イベントハンドラーがほとんどの表示可能なページ要素に対して登録されたとき
        - 目安として5秒以内
        - 詳細は[仕様書で](https://github.com/WICG/time-to-interactive#definition)

---

##### 改善案

- （たぶんいままでのメトリクスを改善すれば勝手に向上する）

---

#### Lighthouseでのみかた

- TTI

---

### Max Potential First Input Delay

- 何かしらのユーザ操作（リンクをクリック、ボタンをタップ、JavaSciptを用いた操作等）が行われてから反応が帰ってくるまでの時間の内、もっとも長い時間
    - ブラウザーのメインスレッドが、ページコンテンツがすでに読み込まれた後（First Meaningful Paintからトレースの終わりまで。これはTime to Interactiveの約5秒後のタイミング）大量のJavaScriptの解析と実行でビジーな場合、低速（er）FIDが発生します

---

#### 改善方法

- [レンダリング パフォーマンス](https://developers.google.com/web/fundamentals/performance/rendering/)を最適化する
- アプリがユーザー入力により速く応答するようにするには、ブラウザーでのコードの実行方法を最適化する必要がある
- [主にSSR SPAで遅くなりがち](https://bitsofco.de/what-is-first-input-delay/)

---

#### Lighthouseでのみかた

- Performanceパネルでもっさりしそうなところを計測し、メインスレッドでいろいろ大変そうなとこを見てみる

